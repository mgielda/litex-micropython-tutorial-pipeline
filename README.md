LiteX tutorial pipeline
=======================

TODO
----

* [x] make script end and test complete
* [x] fetch tutorial from wiki with wget
* [x] just name the scripts in the meta data
* [x] extend parse.py with ability to follow a "spec", like a YAML/ini file with list of snippets and additional commands between them, ability to modify snippets etc
* [ ] simplify naming of scripts after discussion
* [x] make PR to LiteX tutorial with names
* [x] put parse.py on GitHub
* [ ] add internal CI
